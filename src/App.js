import React from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import './App.css';

import Home from './pages/Home/Home'
import Robot from './pages/Robot/Robot'
import TodoList from './pages/TodoList/TodoList'
import Hooks from './pages/Hooks/Experiment'
import ColorLight from './pages/ColorLight/ColorLight'

function App() {
  return (
    <Router>
      <div>
        <Route exact path="/" component={Home} />
        <Route path="/robot" component={Robot} />
        <Route path="/todolist" component={TodoList} />
        <Route path="/hooks" component={Hooks} />
        <Route path="/colorlight" component={ColorLight} />
      </div>
    </Router>
  );
}

export default App;
