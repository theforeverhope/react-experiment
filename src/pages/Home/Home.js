import React from 'react';
import {Link} from "react-router-dom";

function Home() {
  return (
    <ul>
      <li>
        <Link to="/robot">Robot</Link>
      </li>
      <li>
        <Link to="/todolist">TodoList</Link>
      </li>
      <li>
        <Link to="/colorlight">ColorLight</Link>
      </li>
    </ul>
  );
}

export default Home;