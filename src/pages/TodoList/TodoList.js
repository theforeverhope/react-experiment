import React, { useState, useEffect, useCallback } from 'react'
import './TodoList.css'
import { Input } from 'antd'
import Todo from './Todo'
import Button from './Button'


function TodoList() {
  const [value, updateValue] = useState("")
  const [list, updateList] = useState([])
  const [todos, updateTodos] = useState([])
  const [status, updateStatus] = useState('all')

  const addTodo = (event) => {
    if (event.keyCode === 13) {
      updateTodos(todos.concat(
        {
          id: uuid(), 
          title: event.target.value,
          complete: false
        }
      ))
      
      updateValue('')
    }
  }

  const toggleTodo = (id) => {
    const index = todos.findIndex(item => item.id === id)
    updateTodos([
      ...todos.slice(0, index),
      {
        ...todos[index],
        complete: !todos[index].complete
      },
      ...todos.slice(index + 1)
    ])
  }

  const removeTodo = (id) => {
    const index = todos.findIndex(item => item.id === id)
    updateTodos([
      ...todos.slice(0, index),
      ...todos.slice(index + 1)
    ])
  }

  const filterTodos = useCallback((status, todos) => {
    if (status === 'all') {
      updateList(todos)
    } else if (status === 'pending') {
      updateList(todos.filter((todo) => {
        return !todo.complete
      }))
    } else {
      updateList(todos.filter((todo) => {
        return todo.complete
      }))
    }
  }, [])

  useEffect(() => {
    filterTodos(status, todos)
  }, [todos, status, filterTodos])

  return (
    <div className="container">
      <Input 
        value={value} 
        onChange={({ target: { value }}) => updateValue(value)} 
        onKeyDown={addTodo}/>
      <div>
        {
          list.map(todo => {
            return <Todo item={todo} toggle={toggleTodo} remove={removeTodo}></Todo>
          })
        }
      </div>

      <div className="footer">
        <Button 
          type={'home'}
          status={status === 'all'}
          update={() => updateStatus('all')}
          />
        <Button 
          type={'frown'}
          status={status === 'pending'}
          update={() => updateStatus('pending')}
          />
        <Button 
          type={'smile'}
          status={status === 'complete'}
          update={() => updateStatus('complete')}
          />
      </div>
    </div>
  );
}

function uuid () {
  /*jshint bitwise:false */
  var i, random;
  var uuid = '';

  for (i = 0; i < 32; i++) {
    random = Math.random() * 16 | 0;
    if (i === 8 || i === 12 || i === 16 || i === 20) {
      uuid += '-';
    }
    // eslint-disable-next-line no-mixed-operators
    uuid += (i === 12 ? 4 : (i === 16 ? (random & 3 | 8) : random))
      .toString(16);
  }

  return uuid;
}

export default TodoList;