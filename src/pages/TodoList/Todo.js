import React from 'react'
import { Icon } from 'antd'
import './TodoList.css'

class Todo extends React.Component {

  render () {
    return (
      <div 
        key={this.props.item.id} 
        className={(this.props.item.complete ? "complete" : '') + ' item'}
        onClick={() => {this.props.toggle(this.props.item.id)}}>
        { 
          this.props.item.complete ? 
          <Icon type="smile" style={{color: 'orange'}}/> : 
          <Icon type="meh" style={{color: 'blue'}}/>
        }
        <span style={{margin: 20}}>
          {this.props.item.title}
        </span>
        <Icon 
          type="close-circle" 
          style={{float: 'right'}}
          onClick={(e) => {
            e.preventDefault()
            e.stopPropagation()
            this.props.remove(this.props.item.id)
          }}/>
      </div>
    )
  }
}

export default Todo;