import React from 'react'
import { Icon } from 'antd';
import './TodoList.css'

class Button extends React.Component {

  render () {
    return (
      <Icon 
        type={this.props.type} 
        theme="twoTone" 
        twoToneColor={this.props.status ? "#52c41a" : "#a9a9a9"} 
        onClick={() => {this.props.update()}}/>
    )
  }
}

export default Button