import React from 'react';

class Robot extends React.Component {
  constructor(props) {
    super(props);
    this.wsUri = "ws://30.208.32.107:8081";
    this.websocket = new WebSocket(this.wsUri);
    this.state = {
      messages: []
    } 
  }

  componentDidMount () {
    this.testWebSocket()
  }

  testWebSocket = () => {
      this.websocket.onopen = (evt) => {
        this.onOpen(evt)
      };
      this.websocket.onclose = (evt) => {
        this.onClose(evt)
      };
      this.websocket.onmessage = (evt) => {
        this.onMessage(evt)
      };
      this.websocket.onerror = (evt) => {
        this.onError(evt)
      };
  } 

  onOpen = (evt) => {
    this.writeToScreen("CONNECTED");
    this.doSend("WebSocket rocks");
  } 

  onClose = (evt) => {
    this.writeToScreen("DISCONNECTED");
  } 

  onMessage = (evt) => {
    console.log(evt.data)
    this.writeToScreen("Response: " + evt.data);
    // if (evt.data === 'close') {
    // 	websocket.close();
    // }
  } 

  onError = (evt) => {
    this.writeToScreen(evt.data);
  } 

  doSend = (message) => {
    this.writeToScreen("Sent: " + message); 
    this.websocket.send(message);
  } 

  writeToScreen = (message) => {
    this.setState(
      {
        messages: [
          ...this.state.messages,
          message
        ]
      }
    )
  } 

  send = (event) => {
    if (event.keyCode === 13) {
      this.doSend(event.target.value)
    }
  }

  render () {
    return (
      <div className="Robot">
        <div>
          <input type="text" onKeyDown={this.send} />
          { this.state.messages.map((message, index) => {
              return <p key={index}>{message}</p>
            })
          }
        </div> 
      </div>
    );
  }
}

export default Robot;
