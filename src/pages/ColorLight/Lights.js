import React from 'react'
import { Button } from 'antd';

function Light(props) {
  
  const lightColor = ['#FF0000','#FFFF00','#00FF00']
  return (
    <div className="right">
      {
        props.light.map((item, index) => {
          return <div key={index} className="circle" { ...{
            style: item !== 0 ? {
              backgroundColor: lightColor[index]
            } : null
          }
          }></div>
        })
      }
    </div>
  )
}

export default Light;