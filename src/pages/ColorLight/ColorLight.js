import React, {useState, useEffect} from 'react'
import { Button } from 'antd'
import './ColorLight.css'
import Lights from './Lights'

function Panel() {
  const [button, updateButton] = useState("Start Light")
  const [light, updateLight] = useState([1, 0, 0])
  const [interval, updateInterval] = useState(0)
  const lightColor = ['#FF0000','#FFFF00','#00FF00']
  const [color, updateColor] = useState([])

  const controlLight = () => {
    updateButton(button === 'Start Light' ? 'Stop Light' : 'Start Light')
    if (button === 'Stop Light' && light !== 0) {
      clearInterval(interval)
    } else {
      updateInterval(setInterval(() => {
        let temp = light.pop()
        light.unshift(temp)
        updateLight(light.slice())
      }, 1000))
    }
  }

  const addColor = () => {
    const colorIndex = color.length % 3
    updateColor([...color, lightColor[colorIndex]])
  }

  return (
    <div className="container">
      <div className="panel">
        <div className="row">
          <div className="left">
            <Button
              type="primary" 
              onClick={controlLight}>
              { button }
            </Button>
          </div>
          <Lights light={light}></Lights>
        </div>
        <div className="row">
          <div className="left">
            <Button type="primary" onClick={addColor}>Add Color</Button>
          </div>
          <div className="right">
            {
              color.map((c, index) => {
                return <div key={index} className="circle" style={{backgroundColor: c}}></div>
              })
            }
          </div>
        </div>
      </div>
    </div>
  )
}

export default Panel;