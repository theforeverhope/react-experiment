import React from 'react';
import { useState } from './Hooks'

function HooksExperiment() {
  const [firstName, setFirstName] = useState('First Name')
  const [lastName, setLastName] = useState('Last Name')

  return (
    <div>
      <div onClick={() => setFirstName('Shira')}>{firstName}</div>
      <div onClick={() => setLastName('Zhong')}>{lastName}</div>
    </div>
  )
}

export default HooksExperiment

const withState = (st, setSt, initialValue) => Comp => class C extends React.Component {
  state = {
    [st]: initialValue,
    [setSt]: val => {
      this.setState({
        [st]: val,
      });
    },
  }

  render() {
    return <Comp {...this.state} />;
  }
};

const enhance = withState('count', 'setCount', 0);
console.log(enhance)
const Count = enhance(({ count, setCount }) => (
    <div>
      Count: {count}
      <button
        type="button"
        onClick={() => {
          setCount(count + 1);
        }}>Increnment</button>
    </div>
  ));