let state = [];
let setters = [];
let firstRun = [];
let cursor = 0;

function createSetter(cursor) {
  return function setterWithCursor(newVal) {
    state[cursor] = newVal;
    console.log(cursor)
    console.log(newVal)
  };
}

// This is the pseudocode for the useState helper
export function useState(initVal) {
  if (firstRun[cursor] === undefined) {
    state.push(initVal);
    setters.push(createSetter(cursor));
    firstRun.push(false);
  }

  const setter = setters[cursor];
  const value = state[cursor];

  cursor++;
  return [value, setter];
}